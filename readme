Overview on solution:
This is a simple spring boot application which is intend to provide REST APIs to solve calculation-post problem statement,
application has been built to support basic Addition, subtraction, division and multiplication and each one of it uses
handler of it's own which makes easy to add any new operator easily and extend the operator set.

Application has been modeled by keeping in focus of future extension of posting multiple entities though current requirement
specifies only posting of calculations.

Login/Logout and Active session:
Currently there is no authentication built, so it acts as an public form any user can just login by providing user name(without
registration/ sign in and assumes username as unique), as soon user logs in user name added to active session list and custom
session id will be generated and sent back to client, currently session id is not being used, it's been built just by keeping
session out functionality or for any post authentication process.
User can logout at any movement so user will be removed from active session and no post will be captured against user.

Active Session and posts:
As soon user logins, user is considered as an active session holder until user logs out(no session time out been managed
but modeled to handle this feature) and any posts that are posted till user session is active those posts are capture against
user and saved in active post collection, as soon user logs out post against user will gets delete and user session will be killed.

Posts are built in generic so as per current requirement this is only one kind of post which is calculation but it has been modeled
to support other kind of posts with minimal changes.

Supported APIs:
Login User:
    POST: http://localhost:8080/user/login
           body:{
                    "userName": "stranger"
                }
Logout User:
     POST: http://localhost:8080/user/{userName}/logout

Create and Evaluate Expression
    POST: http://localhost:8080/expression/evaluate
            body:{
                     "operand1":"1",
                     "operand2":"2",
                     "operator":"ADD"
                 }

View Evaluated Expression:
    GET: http://localhost:8080/expression/{expressionId}

Create a post:
    POST: http://localhost:8080/post
        body:{
                 "postedBy":"someUser",
                 "postType":"EXPRESSION",
                 "postEntityId":"5fb4152d64b54a3b206c98b4"
             }

View all active posts of specific user:
    GET:http://localhost:8080/active-post?userName=stranger&page=0&size=10

Tech-stack used:
    Java 8
    MongoDB
    Junit
    Spring-boot

Best practices/Principles tried following:
    TDD
    REST endpoints
    SOLID
    API-Integration testing

What could be better:
    Few places modeling has been over engineered and could be made simple to fit current use case.
    Could be made use of generated sessionId in few places.
    Could be considered any unique attribute to identify user rather than name itself.

