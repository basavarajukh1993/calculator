package com.sezzle.calculator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sezzle.calculator.model.User;
import com.sezzle.calculator.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
class UserControllerEndpointTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService service;

    @Test
    void shouldAllowUserToLogin() throws Exception {
        User user = new User("userName");
        when(this.service.userLogin(user.getUserName())).thenReturn("sessionId");

        mockMvc.perform(post("/user/login")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(user))).
                andExpect(status().isOk());

        verify(this.service, times(1)).userLogin(user.getUserName());
    }

    @Test
    void shouldAllowUserToLogout() throws Exception {
        String userName = "userName";
        doNothing().when(this.service).userLogout(userName);

        mockMvc.perform(post("/user/" + userName + "/logout")
                .contentType("application/json")).
                andExpect(status().isOk());

        verify(this.service, times(1)).userLogout(userName);
    }

}
