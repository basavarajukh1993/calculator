package com.sezzle.calculator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.model.Operator;
import com.sezzle.calculator.service.ExpressionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ExpressionController.class)
class ExpressionControllerEndpointTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ExpressionService service;

    @Test
    void shouldEvaluateGivenExpressionWhenInputsAreValid() throws Exception {
        Expression expression = new Expression("1", "1", Operator.ADD);
        when(this.service.evaluate(expression)).thenReturn(expression);

        mockMvc.perform(post("/expression/evaluate")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(expression))).
                andExpect(status().isOk());

        verify(this.service, times(1)).evaluate(expression);
    }

    @Test
    void shouldThrowBadRequestOnInvalidOperands() throws Exception {
        Expression expression = new Expression(null, "1", Operator.ADD);
        doThrow(IllegalArgumentException.class).when(this.service).evaluate(expression);

        mockMvc.perform(post("/expression/evaluate")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(expression))).
                andExpect(status().isBadRequest());

        verify(this.service, times(1)).evaluate(expression);
    }

    @Test
    void shouldThrowBadRequestOnInvalidOperator() throws Exception {
        Expression expression = new Expression("2", "1", null);
        doThrow(UnsupportedOperationException.class).when(this.service).evaluate(expression);

        mockMvc.perform(post("/expression/evaluate")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(expression))).
                andExpect(status().isBadRequest());

        verify(this.service, times(1)).evaluate(expression);
    }

    @Test
    void shouldThrowBadRequestOnArithmeticExceptions() throws Exception {
        Expression expression = new Expression("1", "0", Operator.DIVIDE);
        doThrow(ArithmeticException.class).when(this.service).evaluate(expression);

        mockMvc.perform(post("/expression/evaluate")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(expression))).
                andExpect(status().isBadRequest());

        verify(this.service, times(1)).evaluate(expression);
    }

    @Test
    void shouldGetExpressionById() throws Exception {
        Expression expression = new Expression("1", "1", Operator.ADD);
        expression.setId("expressionId");
        when(this.service.getExpressionById("expressionId")).thenReturn(Optional.of(expression));

        mockMvc.perform(get("/expression/expressionId")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(expression))).
                andExpect(status().isOk());

        verify(this.service, times(1)).getExpressionById("expressionId");
    }

    @Test
    void shouldThrowNotFoundWhenNoExpressionFoundForId() throws Exception {
        Expression expression = new Expression("1", "1", Operator.ADD);
        expression.setId("expressionId");
        when(this.service.getExpressionById("expressionId")).thenReturn(Optional.empty());

        mockMvc.perform(get("/expression/expressionId")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(expression))).
                andExpect(status().isNotFound());

        verify(this.service, times(1)).getExpressionById("expressionId");
    }

}
