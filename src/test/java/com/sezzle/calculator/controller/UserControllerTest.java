package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.User;
import com.sezzle.calculator.service.UserService;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class UserControllerTest {
    @Mock
    private UserService userService;
    private UserController controller;

    @BeforeEach
    void setUp() {
        this.controller = new UserController(this.userService);
    }

    @Test
    void shouldAllowUserToLogin(@Random User userName) {
        String sessionId = "sessionId";
        when(this.userService.userLogin(userName.getUserName())).thenReturn(sessionId);

        ResponseEntity<String> responseEntity = this.controller.userLogin(userName);

        assertThat(responseEntity.getBody(), is(sessionId));
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    void shouldAllowUserToLogout(@Random String userName) {
        doNothing().when(this.userService).userLogout(userName);

        ResponseEntity<String> responseEntity = this.controller.userLogout(userName);

        verify(this.userService, times(1)).userLogout(userName);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

}
