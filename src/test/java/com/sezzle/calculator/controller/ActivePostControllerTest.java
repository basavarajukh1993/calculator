package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.service.ActivePostService;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class ActivePostControllerTest {
    @Mock
    private ActivePostService service;
    private ActivePostController controller;

    @BeforeEach
    void setUp() {
        this.controller = new ActivePostController(this.service);
    }

    @Test
    void shouldGetActivePostForUser(@Random String userName, @Random ActivePost post) {
        ArrayList<ActivePost> activePosts = new ArrayList<ActivePost>() {{
            add(post);
        }};
        when(this.service.getLatestPostsForUser(userName, 0, 10)).thenReturn(activePosts);

        ResponseEntity responseEntity = this.controller.getActivePost(userName, 0, 10);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(responseEntity.getBody(), is(activePosts));
    }
}
