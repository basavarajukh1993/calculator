package com.sezzle.calculator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.service.ActivePostService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ActivePostController.class)
class ActivePostControllerEndpointTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ActivePostService service;

    @Test
    void shouldGetActivePostForUser() throws Exception {
        ActivePost activePost = new ActivePost("postId", "userName");
        ArrayList<ActivePost> activePosts = new ArrayList<ActivePost>() {{
            add(activePost);
        }};
        when(this.service.getLatestPostsForUser("userName", 0, 10)).thenReturn(activePosts);

        mockMvc.perform(get("/active-post?userName=userName&page=0&size=10")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(activePosts))).
                andExpect(status().isOk());

        verify(this.service, times(1)).getLatestPostsForUser("userName", 0, 10);
    }

}
