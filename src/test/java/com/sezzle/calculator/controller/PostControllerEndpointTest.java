package com.sezzle.calculator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sezzle.calculator.model.Post;
import com.sezzle.calculator.model.PostType;
import com.sezzle.calculator.service.PostService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PostController.class)
class PostControllerEndpointTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PostService service;

    @Test
    void shouldCreatePost() throws Exception {
        Post post = new Post("postedUser", PostType.EXPRESSION, "postId");
        when(this.service.createPost(post)).thenReturn(post);

        mockMvc.perform(post("/post/")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(post))).
                andExpect(status().isAccepted());

        verify(this.service, times(1)).createPost(post);
    }

    @Test
    void shouldGetPostById() throws Exception {
        Post post = new Post("postedUser", PostType.EXPRESSION, "postId");
        when(this.service.getPostById("postId")).thenReturn(Optional.of(post));

        mockMvc.perform(get("/post/postId")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(post))).
                andExpect(status().isOk());

        verify(this.service, times(1)).getPostById("postId");
    }

}
