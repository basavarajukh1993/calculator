package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.service.ExpressionService;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class ExpressionControllerTest {
    @Mock
    private ExpressionService service;
    private ExpressionController controller;

    @BeforeEach
    void setUp() {
        this.controller = new ExpressionController(this.service);
    }

    @Test
    void shouldEvaluateGivenExpression(@Random Expression expression, @Random Expression evaluatedExpression) {
        when(this.service.evaluate(expression)).thenReturn(evaluatedExpression);

        ResponseEntity<Expression> responseEntity = this.controller.evaluate(expression);

        assertThat(responseEntity.getBody(), is(evaluatedExpression));
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    void shouldReturnBadRequestWhenIllegalArgumentExceptionRaises(@Random Expression expression) {
        doThrow(IllegalArgumentException.class).when(this.service).evaluate(expression);

        ResponseEntity<Expression> responseEntity = this.controller.evaluate(expression);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void shouldReturnBadRequestWhenUnsupportedOperationExceptionRaises(@Random Expression expression) {
        doThrow(UnsupportedOperationException.class).when(this.service).evaluate(expression);

        ResponseEntity<Expression> responseEntity = this.controller.evaluate(expression);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void shouldReturnBadRequestWhenArithmeticExceptionRaises(@Random Expression expression) {
        doThrow(ArithmeticException.class).when(this.service).evaluate(expression);

        ResponseEntity<Expression> responseEntity = this.controller.evaluate(expression);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    void shouldGetExpressionById(@Random String expressionId, @Random Expression expression) {
        when(this.service.getExpressionById(expressionId)).thenReturn(Optional.of(expression));

        ResponseEntity<Expression> responseEntity = this.controller.getExpressionById(expressionId);

        assertThat(responseEntity.getBody(), is(expression));
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    void shouldNotFoundWhenExpressionNotExist(@Random String expressionId) {
        when(this.service.getExpressionById(expressionId)).thenReturn(Optional.empty());

        ResponseEntity<Expression> responseEntity = this.controller.getExpressionById(expressionId);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

}
