package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.Post;
import com.sezzle.calculator.service.PostService;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class PostControllerTest {
    @Mock
    private PostService service;
    private PostController controller;

    @BeforeEach
    void setUp() {
        this.controller = new PostController(this.service);
    }

    @Test
    void shouldCreatePost(@Random Post post) {
        when(this.service.createPost(post)).thenReturn(post);

        ResponseEntity<Post> responseEntity = this.controller.createPost(post);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.ACCEPTED));
    }

    @Test
    void shouldGetPostById(@Random String postId, @Random Post post) {
        when(this.service.getPostById(postId)).thenReturn(Optional.of(post));

        ResponseEntity<Post> responseEntity = this.controller.getPostById(postId);

        assertThat(responseEntity.getBody(), is(post));
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    void shouldRespondEntityNotFoundWhenNoPostFound(@Random String postId) {
        when(this.service.getPostById(postId)).thenReturn(Optional.empty());

        ResponseEntity<Post> responseEntity = this.controller.getPostById(postId);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

}
