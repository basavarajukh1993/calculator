package com.sezzle.calculator.Notifier;

import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.service.ActivePostService;
import com.sezzle.calculator.service.UserService;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class PostNotifierTest {
    @Mock
    private UserService userService;
    @Mock
    private ActivePostService activePostService;
    private PostNotifier notifier;

    @BeforeEach
    void setUp() {
        this.notifier = new PostNotifier(this.userService, this.activePostService);
    }

    @Test
    void shouldNotifyAboutPostToActiveUsers(@Random String postId) {
        Set<String> activeUsers = new HashSet<>();
        activeUsers.add("user1");
        activeUsers.add("user2");

        ActivePost activePost1 = new ActivePost(postId, "user1");
        ActivePost activePost2 = new ActivePost(postId, "user2");

        when(this.userService.getActiveUsers()).thenReturn(activeUsers);
        when(this.activePostService.createActivePost(activePost1)).thenReturn(activePost1);
        when(this.activePostService.createActivePost(activePost2)).thenReturn(activePost2);

        this.notifier.notifyActiveUsers(postId);

        verify(this.activePostService, times(1)).createActivePost(activePost1);
        verify(this.activePostService, times(1)).createActivePost(activePost2);
    }


}
