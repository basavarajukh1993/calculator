package com.sezzle.calculator.repository;

import com.sezzle.calculator.model.ActivePost;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RandomBeansExtension.class)
class ActivePostRepositoryTest {
    @Autowired
    private ActivePostRepository repository;
    @Autowired
    private MongoTemplate template;

    @BeforeEach
    public void clear() {
        template.getCollection("activePost").drop();
    }

    @Test
    void shouldAddActivePost(@Random ActivePost post) {
        ActivePost createdPost = repository.insert(post);

        assertNotNull(createdPost.getId());
        assertNotNull(repository.findById(createdPost.getId()));
    }

    @Test
    void ShouldGetTopTenActivePostForUser(@Random ActivePost post1, @Random ActivePost post2,
                                          @Random ActivePost post3, @Random ActivePost post4,
                                          @Random ActivePost post5, @Random ActivePost post6,
                                          @Random ActivePost post7, @Random ActivePost post8,
                                          @Random ActivePost post9, @Random ActivePost post10,
                                          @Random ActivePost post11, @Random ActivePost post12) {
        post1.setPostId("postId1");
        post1.setActiveUserName("activeUserId");
        this.repository.insert(post1);

        post2.setPostId("postId2");
        post2.setActiveUserName("activeUserId");
        this.repository.insert(post2);

        post3.setPostId("postId3");
        post3.setActiveUserName("activeUserId");
        this.repository.insert(post3);

        post4.setPostId("postId4");
        post4.setActiveUserName("activeUserId");
        this.repository.insert(post4);

        post5.setPostId("postId5");
        post5.setActiveUserName("activeUserId");
        this.repository.insert(post5);

        post6.setPostId("postId6");
        post6.setActiveUserName("activeUserId");
        this.repository.insert(post6);

        post7.setPostId("postId7");
        post7.setActiveUserName("activeUserId");
        this.repository.insert(post7);

        post8.setPostId("postId8");
        post8.setActiveUserName("activeUserId");
        this.repository.insert(post8);

        post9.setPostId("postId9");
        post9.setActiveUserName("activeUserId");
        this.repository.insert(post9);

        post10.setPostId("postId10");
        post10.setActiveUserName("activeUserId");
        this.repository.insert(post10);

        post11.setPostId("postId11");
        post11.setActiveUserName("activeUserId");
        this.repository.insert(post11);

        post12.setPostId("postId12");
        post12.setActiveUserName("activeUserId");
        this.repository.insert(post12);

        List<ActivePost> activeUserId = this.repository.findByActiveUserNameOrderByIdDesc("activeUserId", PageRequest.of(0,10));

        assertThat(activeUserId.size(), is(10));
    }

    @Test
    void shouldDeletePostsForSpecifiedUser(@Random String userName, @Random ActivePost post1, @Random ActivePost post2,
                                           @Random ActivePost post3) {
        post1.setActiveUserName(userName);
        post2.setActiveUserName(userName);
        this.repository.insert(post1);
        this.repository.insert(post2);
        this.repository.insert(post3);

        this.repository.deleteAllByActiveUserName(userName);

        List<ActivePost> allPosts = this.repository.findAll();
        assertThat(allPosts.size(), is(1));
        assertThat(allPosts.get(0), is(post3));
    }
}

