package com.sezzle.calculator.repository;

import com.sezzle.calculator.model.Expression;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RandomBeansExtension.class)
class ExpressionRepositoryTest {
    @Autowired
    private ExpressionRepository repository;
    @Autowired
    private MongoTemplate template;

    @BeforeEach
    public void clear() {
        template.getCollection("expression").drop();
    }

    @Test
    void shouldInsertExpression(@Random Expression expression) {
        Expression savedExpression = repository.insert(expression);

        assertNotNull(savedExpression.getId());
        assertNotNull(repository.findById(savedExpression.getId()));
    }

    @Test
    void shouldGetExpressionById(@Random Expression sampleExpression) {
        repository.insert(sampleExpression);

        Optional<Expression> byId = repository.findById(sampleExpression.getId());

        assertTrue(byId.isPresent());
        assertThat(byId.get(), is(sampleExpression));
    }

    @Test
    void shouldGetExpressionById(@Random String expressionId, @Random Expression expression) {
        expression.setId(expressionId);
        this.repository.insert(expression);

        Optional<Expression> byId = this.repository.findById(expressionId);

        assertTrue(byId.isPresent());
        assertThat(byId.get(), is(expression));
    }
}
