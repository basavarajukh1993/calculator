package com.sezzle.calculator.repository;

import com.sezzle.calculator.model.Post;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(RandomBeansExtension.class)
class PostRepositoryTest {
    @Autowired
    private PostRepository repository;
    @Autowired
    private MongoTemplate template;

    @BeforeEach
    public void clear() {
        template.getCollection("post").drop();
    }

    @Test
    void shouldCreateAPost(@Random Post post) {
        Post createdPost = repository.insert(post);

        assertNotNull(createdPost.getId());
        assertNotNull(repository.findById(createdPost.getId()));
    }

    @Test
    void shouldGetPostByEntityId(@Random Post post, @Random Post otherPost) {
        this.repository.insert(post);
        this.repository.insert(otherPost);

        Post byPostEntityId = this.repository.findByPostEntityId(post.getPostEntityId());

        assertThat(byPostEntityId, is(post));
    }

    @Test
    void shouldGetPostById(@Random Post post) {
        this.repository.insert(post);

        Optional<Post> byId = this.repository.findById(post.getId());

        Assertions.assertTrue(byId.isPresent());
        assertThat(byId.get(), is(post));
    }
}
