package com.sezzle.calculator.handler;

import com.sezzle.calculator.service.ActivePostService;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class LogoutHandlerTest {
    @Mock
    private ActivePostService activePostService;
    private LogoutHandler handler;

    @BeforeEach
    void setUp() {
        this.handler = new LogoutHandler(this.activePostService);
    }

    @Test
    void shouldCleanUpActivePostForLoggedOutUser(@Random String userName) {
        doNothing().when(this.activePostService).deletePostsForUser(userName);

        this.handler.handle(userName);

        verify(this.activePostService, times(1)).deletePostsForUser(userName);
    }
}
