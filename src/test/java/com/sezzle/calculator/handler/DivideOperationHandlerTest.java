package com.sezzle.calculator.handler;

import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.model.Operator;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(RandomBeansExtension.class)
class DivideOperationHandlerTest {
    private OperationHandler handler;

    @BeforeEach
    void setUp() {
        this.handler = new DivideOperationHandler();
    }

    @Test
    void shouldProvidedQuotientAfterDividingOperandsOnlyWhenOperandsAreInteger(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("2");
        expression.setOperator(Operator.DIVIDE);

        assertThat(this.handler.process(expression), is("0"));

        expression.setOperand1("-1");
        expression.setOperand2("2");

        assertThat(this.handler.process(expression), is("0"));

        expression.setOperand1("-2");
        expression.setOperand2("-2");

        assertThat(this.handler.process(expression), is("1"));

        expression.setOperand1("-2");
        expression.setOperand2("1");

        assertThat(this.handler.process(expression), is("-2"));

        expression.setOperand1("0");
        expression.setOperand2("1");

        assertThat(this.handler.process(expression), is("0"));
    }

    @Test
    void shouldDivideByZeroExceptionWhenProvidedDenominatorOperandIsZero(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("0");
        expression.setOperator(Operator.DIVIDE);

        assertThrows(ArithmeticException.class, () -> this.handler.process(expression));
    }


    @Test
    void shouldThrowUnsupportedExceptionWhenOperandsAreNonNumeric(@Random Expression expression) {
        expression.setOperator(Operator.DIVIDE);
        expression.setOperand1("A");
        expression.setOperand2("1");

        assertThrows(UnsupportedOperationException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowUnsupportedExceptionWhenProvidedOperandsIsDouble(@Random Expression expression) {
        expression.setOperand1("1.1");
        expression.setOperand2("2.1");
        expression.setOperator(Operator.DIVIDE);

        assertThrows(UnsupportedOperationException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenProvidedOperandIsNull(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2(null);
        expression.setOperator(Operator.DIVIDE);

        assertThrows(IllegalArgumentException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenProvidedOperandIsEmpty(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("");
        expression.setOperator(Operator.DIVIDE);

        assertThrows(IllegalArgumentException.class, () -> this.handler.process(expression));
    }

}
