package com.sezzle.calculator.handler;

import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.model.Operator;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(RandomBeansExtension.class)
class AddOperationHandlerTest {
    private OperationHandler handler;

    @BeforeEach
    void setUp() {
        this.handler = new AddOperationHandler();
    }

    @Test
    void shouldProvidedSumOfTwoOperandsWhenProvidedOperandsIsInteger(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("2");
        expression.setOperator(Operator.ADD);

        assertThat(this.handler.process(expression), is("3"));

        expression.setOperand1("-1");
        expression.setOperand2("2");

        assertThat(this.handler.process(expression), is("1"));

        expression.setOperand1("-1");
        expression.setOperand2("-2");

        assertThat(this.handler.process(expression), is("-3"));
    }

    @Test
    void shouldReturnConcatStringOfOperandsWhenOperandsAreNotNumeric(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("A");
        expression.setOperator(Operator.ADD);

        String result = this.handler.process(expression);

        assertThat(result, is("1A"));
    }

    @Test
    void shouldThrowUnsupportedExceptionWhenProvidedOperandsIsDouble(@Random Expression expression) {
        expression.setOperand1("1.1");
        expression.setOperand2("2.1");
        expression.setOperator(Operator.ADD);

        assertThrows(UnsupportedOperationException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenProvidedOperandIsNull(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2(null);
        expression.setOperator(Operator.ADD);

        assertThrows(IllegalArgumentException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenProvidedOperandIsEmpty(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("");
        expression.setOperator(Operator.ADD);

        assertThrows(IllegalArgumentException.class, () -> this.handler.process(expression));
    }

}
