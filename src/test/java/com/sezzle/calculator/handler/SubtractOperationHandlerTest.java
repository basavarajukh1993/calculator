package com.sezzle.calculator.handler;

import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.model.Operator;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(RandomBeansExtension.class)
class SubtractOperationHandlerTest {
    private SubtractOperationHandler handler;

    @BeforeEach
    void setUp() {
        this.handler = new SubtractOperationHandler();
    }

    @Test
    void shouldSubtractOperandsWhenBothTheOperandsAreIntegerNumbers(@Random Expression expression) {
        expression.setOperator(Operator.SUBTRACT);
        expression.setOperand1("2");
        expression.setOperand2("1");

        assertThat(this.handler.process(expression), is("1"));

        expression.setOperand1("1");
        expression.setOperand2("2");

        assertThat(this.handler.process(expression), is("-1"));

        expression.setOperand1("-1");
        expression.setOperand2("-2");

        assertThat(this.handler.process(expression), is("1"));

    }

    @Test
    void shouldThrowUnsupportedExceptionWhenOperandsAreNonNumeric(@Random Expression expression) {
        expression.setOperator(Operator.SUBTRACT);
        expression.setOperand1("A");
        expression.setOperand2("1");

        assertThrows(UnsupportedOperationException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowUnsupportedExceptionWhenOperandsAreNonInteger(@Random Expression expression) {
        expression.setOperator(Operator.SUBTRACT);
        expression.setOperand1("1.1");
        expression.setOperand2("1");

        assertThrows(UnsupportedOperationException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenProvidedOperandIsNull(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2(null);
        expression.setOperator(Operator.SUBTRACT);

        assertThrows(IllegalArgumentException.class, () -> this.handler.process(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenProvidedOperandIsEmpty(@Random Expression expression) {
        expression.setOperand1("1");
        expression.setOperand2("");
        expression.setOperator(Operator.SUBTRACT);

        assertThrows(IllegalArgumentException.class, () -> this.handler.process(expression));
    }
}
