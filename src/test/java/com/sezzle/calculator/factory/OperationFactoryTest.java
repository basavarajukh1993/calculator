package com.sezzle.calculator.factory;

import com.sezzle.calculator.handler.AddOperationHandler;
import com.sezzle.calculator.handler.DivideOperationHandler;
import com.sezzle.calculator.handler.MultiplyOperationHandler;
import com.sezzle.calculator.handler.SubtractOperationHandler;
import com.sezzle.calculator.model.Operator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.expression.Operation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

class OperationFactoryTest {
    private OperationFactory factory;

    @BeforeEach
    void setUp() {
        this.factory = new OperationFactory();
    }

    @Test
    void shouldGetCorrespondingHandlesBasedOnOperation() {
        assertThat(this.factory.getOperationHandler(Operator.ADD), instanceOf(AddOperationHandler.class));
        assertThat(this.factory.getOperationHandler(Operator.DIVIDE), instanceOf(DivideOperationHandler.class));
        assertThat(this.factory.getOperationHandler(Operator.SUBTRACT), instanceOf(SubtractOperationHandler.class));
        assertThat(this.factory.getOperationHandler(Operator.MULTIPLY), instanceOf(MultiplyOperationHandler.class));
    }
}
