package com.sezzle.calculator.service;

import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(RandomBeansExtension.class)
class SessionServiceTest {
    private SessionService service;

    @BeforeEach
    void setUp() {
        this.service = new SessionService();
    }

    @Test
    void shouldGenerateSessionForUser(@Random String userName) {
        String sessionId = this.service.generateSession(userName);

        assertThat(sessionId.split("&&")[1], is(userName));
    }
}
