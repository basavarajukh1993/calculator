package com.sezzle.calculator.service;

import com.sezzle.calculator.Notifier.PostNotifier;
import com.sezzle.calculator.model.Post;
import com.sezzle.calculator.repository.PostRepository;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class PostServiceTest {
    @Mock
    private PostRepository repository;
    @Mock
    private PostNotifier notifier;
    private PostService service;

    @BeforeEach
    void setUp() {
        this.service = new PostService(this.repository, this.notifier);
    }

    @Test
    void shouldCreateAPostAndNotifyActiveUsers(@Random Post post) {
        when(this.repository.insert(post)).thenReturn(post);
        doNothing().when(this.notifier).notifyActiveUsers(post.getId());

        Post createdPost = this.service.createPost(post);

        assertThat(createdPost, is(post));
        verify(this.notifier, times(1)).notifyActiveUsers(post.getId());
    }

    @Test
    void shouldGetPostById(@Random Post post) {
        when(this.repository.findById(post.getId())).thenReturn(Optional.of(post));

        Optional<Post> postById = this.service.getPostById(post.getId());

        assertTrue(postById.isPresent());
        assertThat(postById.get(), is(post));

    }
}
