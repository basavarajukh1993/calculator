package com.sezzle.calculator.service;

import com.sezzle.calculator.handler.LogoutHandler;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class UserServiceTest {
    @Mock
    private SessionService sessionService;
    @Mock
    private LogoutHandler logoutHandler;
    private UserService userService;

    @BeforeEach
    void setUp() {
        this.userService = new UserService(this.sessionService, this.logoutHandler);
    }

    @Test
    void shouldGetActiveUsers() {
        this.userService.userLogin("user1");
        this.userService.userLogin("user2");

        Set<String> activeUsers = this.userService.getActiveUsers();

        assertThat(activeUsers, hasItem("user1"));
        assertThat(activeUsers, hasItem("user2"));
    }

    @Test
    void shouldLoginAndGenerateSession(@Random String userName) {
        String expectedSessionId = "randomSessionId";
        when(this.sessionService.generateSession(userName)).thenReturn(expectedSessionId);

        String userSessionId = this.userService.userLogin(userName);

        assertThat(userSessionId, is(expectedSessionId));
        assertThat(this.userService.getActiveUsers(), hasItem(userName));
    }

    @Test
    void shouldUpdateSessionIfUserAlreadyLoggedIn(@Random String userName) {
        String session1 = "randomSessionId";
        String session2 = "randomSessionId";
        when(this.sessionService.generateSession(userName)).thenReturn(session1).thenReturn(session2);

        String userSessionId = this.userService.userLogin(userName);

        assertThat(userSessionId, is(session2));
        assertThat(this.userService.getActiveUsers(), hasItem(userName));
    }

    @Test
    void shouldBeAbleToLogoutUser(@Random String userName) {
        String expectedSessionId = "randomSessionId";
        when(this.sessionService.generateSession(userName)).thenReturn(expectedSessionId);
        doNothing().when(this.logoutHandler).handle(userName);
        this.userService.userLogin(userName);

        this.userService.userLogout(userName);

        assertTrue(this.userService.getActiveUsers().isEmpty());
        verify(this.logoutHandler, times(1)).handle(userName);
    }
}
