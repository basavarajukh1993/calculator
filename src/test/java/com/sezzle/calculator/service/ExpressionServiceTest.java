package com.sezzle.calculator.service;

import com.sezzle.calculator.factory.OperationFactory;
import com.sezzle.calculator.handler.AddOperationHandler;
import com.sezzle.calculator.handler.DivideOperationHandler;
import com.sezzle.calculator.handler.SubtractOperationHandler;
import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.model.Operator;
import com.sezzle.calculator.repository.ExpressionRepository;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class ExpressionServiceTest {
    @Mock
    private OperationFactory factory;
    @Mock
    private AddOperationHandler addOperationHandler;
    @Mock
    private DivideOperationHandler divideOperationHandler;
    @Mock
    private SubtractOperationHandler subtractOperationHandler;
    @Mock
    private ExpressionRepository repository;

    private ExpressionService service;

    @BeforeEach
    void setUp() {
        this.service = new ExpressionService(this.factory, this.repository);
    }

    @Test
    void shouldAddOperands(@Random Expression expression) {
        expression.setOperator(Operator.ADD);
        when(this.factory.getOperationHandler(Operator.ADD)).thenReturn(addOperationHandler);
        when(this.addOperationHandler.process(expression)).thenReturn("result");
        when(this.repository.insert(expression)).thenReturn(expression);

        Expression evaluatedExpression = this.service.evaluate(expression);

        assertThat(evaluatedExpression.getResult(), is("result"));
        assertThat(evaluatedExpression, is(expression));
    }

    @Test
    void shouldSubtractOperands(@Random Expression expression) {
        expression.setOperator(Operator.SUBTRACT);
        when(this.factory.getOperationHandler(Operator.SUBTRACT)).thenReturn(subtractOperationHandler);
        when(this.subtractOperationHandler.process(expression)).thenReturn("result");
        when(this.repository.insert(expression)).thenReturn(expression);

        Expression evaluatedExpression = this.service.evaluate(expression);

        assertThat(evaluatedExpression.getResult(), is("result"));
        assertThat(evaluatedExpression, is(expression));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenHandlerRaisesException(@Random Expression expression) {
        expression.setOperator(Operator.SUBTRACT);
        when(this.factory.getOperationHandler(Operator.SUBTRACT)).thenReturn(subtractOperationHandler);
        doThrow(IllegalArgumentException.class).when(this.subtractOperationHandler).process(expression);

        assertThrows(IllegalArgumentException.class, () -> this.service.evaluate(expression));
        verify(this.repository, never()).insert(any(Expression.class));
    }

    @Test
    void shouldThrowUnsupportedOperationExceptionWhenHandlerRaisesException(@Random Expression expression) {
        expression.setOperator(Operator.SUBTRACT);
        when(this.factory.getOperationHandler(Operator.SUBTRACT)).thenReturn(subtractOperationHandler);
        doThrow(UnsupportedOperationException.class).when(this.subtractOperationHandler).process(expression);

        assertThrows(UnsupportedOperationException.class, () -> this.service.evaluate(expression));
        verify(this.repository, never()).insert(any(Expression.class));
    }

    @Test
    void shouldThrowArithmeticExceptionOperationExceptionWhenHandlerRaisesException(@Random Expression expression) {
        expression.setOperator(Operator.DIVIDE);
        when(this.factory.getOperationHandler(Operator.DIVIDE)).thenReturn(divideOperationHandler);
        doThrow(ArithmeticException.class).when(this.divideOperationHandler).process(expression);

        assertThrows(ArithmeticException.class, () -> this.service.evaluate(expression));
        verify(this.repository, never()).insert(any(Expression.class));
    }

    @Test
    void shouldGetExpressionById(@Random String expressionId, @Random Expression expression) {
        when(this.repository.findById(expressionId)).thenReturn(Optional.of(expression));

        Optional<Expression> expressionById = this.service.getExpressionById(expressionId);

        assertTrue(expressionById.isPresent());
        assertThat(expressionById.get(), is(expression));
    }
}
