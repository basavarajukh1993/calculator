package com.sezzle.calculator.service;

import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.repository.ActivePostRepository;
import io.github.glytching.junit.extension.random.Random;
import io.github.glytching.junit.extension.random.RandomBeansExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(RandomBeansExtension.class)
class ActivePostServiceTest {
    @Mock
    private ActivePostRepository repository;
    private ActivePostService service;

    @BeforeEach
    void setUp() {
        this.service = new ActivePostService(this.repository);
    }

    @Test
    void shouldCreateActivePost(@Random ActivePost post) {
        when(this.repository.insert(post)).thenReturn(post);

        ActivePost savedActivePost = this.service.createActivePost(post);

        assertThat(savedActivePost, is(post));
    }

    @Test
    void shouldGetLatestPostsForUser(@Random String userName, @Random ActivePost post) {
        ArrayList<ActivePost> activePosts = new ArrayList<>();
        activePosts.add(post);
        when(this.repository.findByActiveUserNameOrderByIdDesc(userName, PageRequest.of(0, 10))).thenReturn(activePosts);

        List<ActivePost> latestTenPostsForUser = this.service.getLatestPostsForUser(userName, 0, 10);

        assertThat(latestTenPostsForUser, is(activePosts));
    }

    @Test
    void shouldDeletePostRelatedToUser(@Random String userId) {
        doNothing().when(this.repository).deleteAllByActiveUserName(userId);

        this.service.deletePostsForUser(userId);

        verify(this.repository, times(1)).deleteAllByActiveUserName(userId);
    }
}
