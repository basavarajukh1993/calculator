package com.sezzle.calculator.factory;

import com.sezzle.calculator.handler.*;
import com.sezzle.calculator.model.Operator;
import org.springframework.stereotype.Component;

@Component
public class OperationFactory {
    public OperationHandler getOperationHandler(Operator operation) {
        switch (operation) {
            case ADD:
                return new AddOperationHandler();
            case SUBTRACT:
                return new SubtractOperationHandler();
            case DIVIDE:
                return new DivideOperationHandler();
            case MULTIPLY:
                return new MultiplyOperationHandler();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
