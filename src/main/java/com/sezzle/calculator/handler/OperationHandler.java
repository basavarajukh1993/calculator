package com.sezzle.calculator.handler;

import com.sezzle.calculator.model.Expression;

public interface OperationHandler {
    String process(Expression expression);
}
