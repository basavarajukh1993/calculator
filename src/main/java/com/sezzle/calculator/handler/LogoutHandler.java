package com.sezzle.calculator.handler;

import com.sezzle.calculator.service.ActivePostService;
import org.springframework.stereotype.Component;

@Component
public class LogoutHandler {
    private final ActivePostService activePostService;

    public LogoutHandler(ActivePostService activePostService) {
        this.activePostService = activePostService;
    }

    public void handle(String userId) {
        this.activePostService.deletePostsForUser(userId);
    }
}
