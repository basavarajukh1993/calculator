package com.sezzle.calculator.handler;

import com.sezzle.calculator.model.Expression;

public class DivideOperationHandler implements OperationHandler {
    @Override
    public String process(Expression expression) {
        if (!isValidOperands(expression)) throw new IllegalArgumentException("Unsupported arguments");
        try {
            return String.valueOf(Integer.parseInt(expression.getOperand1()) / Integer.parseInt(expression.getOperand2()));
        } catch (NumberFormatException exception) {
            throw new UnsupportedOperationException("Only Integer division is supported");
        } catch (ArithmeticException exception) {
            throw new ArithmeticException("Operand 2/denominator operand should be non-zero");
        }
    }

    private boolean isValidOperands(Expression expression) {
        return !(expression.getOperand1() == null || expression.getOperand2() == null || expression.getOperand1().isEmpty() || expression.getOperand2().isEmpty());
    }
}
