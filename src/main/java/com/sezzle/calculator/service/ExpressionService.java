package com.sezzle.calculator.service;

import com.sezzle.calculator.factory.OperationFactory;
import com.sezzle.calculator.handler.OperationHandler;
import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.repository.ExpressionRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ExpressionService {

    private final OperationFactory factory;
    private final ExpressionRepository repository;

    public ExpressionService(OperationFactory factory, ExpressionRepository repository) {
        this.factory = factory;
        this.repository = repository;
    }

    public Expression evaluate(Expression expression) {
        OperationHandler operationHandler = this.factory.getOperationHandler(expression.getOperator());
        String result = operationHandler.process(expression);
        expression.setResult(result);
        return this.repository.insert(expression);
    }

    public Optional<Expression> getExpressionById(String expressionId) {
        return this.repository.findById(expressionId);
    }
}
