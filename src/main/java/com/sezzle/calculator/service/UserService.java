package com.sezzle.calculator.service;

import com.sezzle.calculator.handler.LogoutHandler;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class UserService {
    private final LogoutHandler logoutHandler;
    private final SessionService sessionService;
    private final Map<String, String> activeUsers;

    public UserService(SessionService sessionService, LogoutHandler logoutHandler) {
        this.sessionService = sessionService;
        this.logoutHandler = logoutHandler;
        activeUsers = new HashMap<>();
    }

    public String userLogin(String userName) {
        String sessionId = this.sessionService.generateSession(userName);
        this.activeUsers.put(userName, sessionId);
        return sessionId;
    }

    public Set<String> getActiveUsers() {
        return this.activeUsers.keySet();
    }

    public void userLogout(String userName) {
        this.activeUsers.remove(userName);
        this.logoutHandler.handle(userName);
    }
}
