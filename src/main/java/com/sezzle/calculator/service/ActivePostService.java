package com.sezzle.calculator.service;

import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.repository.ActivePostRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivePostService {
    private final ActivePostRepository repository;

    public ActivePostService(ActivePostRepository repository) {
        this.repository = repository;
    }

    public ActivePost createActivePost(ActivePost activePost) {
        return this.repository.insert(activePost);
    }

    public List<ActivePost> getLatestPostsForUser(String userName, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        return this.repository.findByActiveUserNameOrderByIdDesc(userName, pageRequest);
    }

    public void deletePostsForUser(String userName) {
        this.repository.deleteAllByActiveUserName(userName);
    }
}
