package com.sezzle.calculator.service;

import com.sezzle.calculator.model.SessionId;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class SessionService {

    public String generateSession(String userName) {
        return new SessionId(LocalDateTime.now(), userName, RandomStringUtils.random(10, true, true)).toString();
    }
}
