package com.sezzle.calculator.service;

import com.sezzle.calculator.Notifier.PostNotifier;
import com.sezzle.calculator.model.Post;
import com.sezzle.calculator.repository.PostRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostService {
    private final PostRepository repository;
    private final PostNotifier notifier;

    public PostService(PostRepository repository, PostNotifier notifier) {
        this.repository = repository;
        this.notifier = notifier;
    }

    public Post createPost(Post post) {
        Post createdPost = this.repository.insert(post);
        this.notifier.notifyActiveUsers(createdPost.getId());
        return createdPost;
    }

    public Optional<Post> getPostById(String postId) {
        return this.repository.findById(postId);
    }
}
