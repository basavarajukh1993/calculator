package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.service.ActivePostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("active-post")
@CrossOrigin("http://localhost:4200")
public class ActivePostController {
    private final ActivePostService service;

    public ActivePostController(ActivePostService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity getActivePost(@RequestParam String userName, @RequestParam int page, @RequestParam int size) {
        List<ActivePost> latestTenPostsForUser = this.service.getLatestPostsForUser(userName, page, size);
        return new ResponseEntity(latestTenPostsForUser, HttpStatus.OK);
    }
}
