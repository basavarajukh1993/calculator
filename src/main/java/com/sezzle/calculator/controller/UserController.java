package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.User;
import com.sezzle.calculator.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("user")
@CrossOrigin("http://localhost:4200")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("login")
    public ResponseEntity<String> userLogin(@RequestBody User user) {
        String sessionId = this.userService.userLogin(user.getUserName());
        return new ResponseEntity<>(sessionId, HttpStatus.OK);
    }

    @PostMapping("{userName}/logout")
    public ResponseEntity<Void> userLogout(@PathVariable String userName) {
        this.userService.userLogout(userName);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
