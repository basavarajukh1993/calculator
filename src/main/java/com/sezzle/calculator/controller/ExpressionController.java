package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.Expression;
import com.sezzle.calculator.service.ExpressionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("expression")
@CrossOrigin("http://localhost:4200")
public class ExpressionController {
    private final ExpressionService service;

    public ExpressionController(ExpressionService service) {
        this.service = service;
    }

    @PostMapping("evaluate")
    public ResponseEntity<Expression> evaluate(@RequestBody Expression expression) {
        try {
            Expression evaluatedExpression = this.service.evaluate(expression);
            return new ResponseEntity<>(evaluatedExpression, HttpStatus.OK);
        } catch (IllegalArgumentException | UnsupportedOperationException | ArithmeticException exception) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("{expressionId}")
    public ResponseEntity<Expression> getExpressionById(@PathVariable String expressionId) {
        Optional<Expression> optionalExpression = this.service.getExpressionById(expressionId);
        return optionalExpression.map(expression -> new ResponseEntity<>(expression, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
