package com.sezzle.calculator.controller;

import com.sezzle.calculator.model.Post;
import com.sezzle.calculator.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("post")
@CrossOrigin("http://localhost:4200")
public class PostController {
    private final PostService service;

    public PostController(PostService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Post> createPost(@RequestBody Post post) {
        this.service.createPost(post);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("{postId}")
    public ResponseEntity<Post> getPostById(@PathVariable String postId) {
        Optional<Post> postById = this.service.getPostById(postId);
        return postById.map(post -> new ResponseEntity<>(post, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
