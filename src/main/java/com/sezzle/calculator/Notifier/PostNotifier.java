package com.sezzle.calculator.Notifier;

import com.sezzle.calculator.model.ActivePost;
import com.sezzle.calculator.service.ActivePostService;
import com.sezzle.calculator.service.UserService;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class PostNotifier {
    private final UserService userService;
    private ActivePostService activePostService;

    public PostNotifier(UserService userService, ActivePostService activePostService) {
        this.userService = userService;
        this.activePostService = activePostService;
    }

    public void notifyActiveUsers(String postId) {
        Set<String> activeUsersIds = this.userService.getActiveUsers();
        for (String userId : activeUsersIds) {
            this.addPost(userId, postId);
        }
    }

    private void addPost(String userId, String postId) {
        this.activePostService.createActivePost(new ActivePost(postId, userId));
    }
}
