package com.sezzle.calculator.repository;

import com.sezzle.calculator.model.Expression;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ExpressionRepository extends MongoRepository<Expression, String> {
}
