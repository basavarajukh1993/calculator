package com.sezzle.calculator.repository;

import com.sezzle.calculator.model.Post;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PostRepository extends MongoRepository<Post, String> {
    Post findByPostEntityId(String entityId);
}
