package com.sezzle.calculator.repository;

import com.sezzle.calculator.model.ActivePost;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ActivePostRepository extends MongoRepository<ActivePost, String>, PagingAndSortingRepository<ActivePost, String> {
    List<ActivePost> findByActiveUserNameOrderByIdDesc(String activeUserName, Pageable pageable);

    void deleteAllByActiveUserName(String activeUserName);
}
