package com.sezzle.calculator.model;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class SessionId {
    private final String userName;
    private final String randomString;
    private final LocalDateTime timeStamp;

    public SessionId(LocalDateTime timeStamp, String userName, String randomString) {
        this.timeStamp = timeStamp;
        this.userName = userName;
        this.randomString = randomString;
    }

    public String getUserName() {
        return userName;
    }

    public String getRandomString() {
        return randomString;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String toString() {
        return this.timeStamp.toEpochSecond(ZoneOffset.MAX) + "&&" + this.userName + "&&" + this.randomString;
    }
}
