package com.sezzle.calculator.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class Post {
    @Id
    private String id;

    private String postedBy;
    private PostType postType;
    private String postEntityId;

    public Post(String postedBy, PostType postType, String postEntityId) {
        this.postedBy = postedBy;
        this.postType = postType;
        this.postEntityId = postEntityId;
    }

    public String getId() {
        return id;
    }

    public PostType getPostType() {
        return postType;
    }

    public void setPostType(PostType postType) {
        this.postType = postType;
    }

    public String getPostEntityId() {
        return postEntityId;
    }

    public void setPostEntityId(String postEntityId) {
        this.postEntityId = postEntityId;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id) &&
                Objects.equals(postedBy, post.postedBy) &&
                postType == post.postType &&
                Objects.equals(postEntityId, post.postEntityId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, postedBy, postType, postEntityId);
    }
}
