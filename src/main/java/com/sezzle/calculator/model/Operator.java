package com.sezzle.calculator.model;

public enum Operator {
    ADD, SUBTRACT, DIVIDE, MULTIPLY
}
