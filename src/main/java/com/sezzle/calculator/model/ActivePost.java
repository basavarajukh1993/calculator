package com.sezzle.calculator.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document
public class ActivePost {
    @Id
    private String id;

    private String postId;
    private String activeUserName;

    public ActivePost(String postId, String activeUserName) {
        this.postId = postId;
        this.activeUserName = activeUserName;
    }

    public String getId() {
        return id;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getActiveUserName() {
        return activeUserName;
    }

    public void setActiveUserName(String activeUserName) {
        this.activeUserName = activeUserName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivePost that = (ActivePost) o;
        return Objects.equals(postId, that.postId) &&
                Objects.equals(activeUserName, that.activeUserName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, activeUserName);
    }
}
